﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Turner.DevChallenge.Model;

namespace Turner.DevChallenge.Web.ViewModels
{
    public class TitleViewModel
    {
        public int TitleId { get; set; }

        public String TitleName { get; set; }

        public String TitleNameSortable { get; set; }

        public int? TitleTypeId { get; set; }

        public int? ReleaseYear { get; set; }

        public DateTime? ProcessedDateTimeUTC { get; set; }

        public List<TitleParticipantViewModel> TitleParticipants { get; set; }

        public List<TitleGenreViewModel> TitleGenres { get; set; }

        public List<StoryLineViewModel> StoryLines { get; set; }

        public List<OtherNameViewModel> OtherNames { get; set; }

        public List<AwardViewModel> Awards { get; set; }

        public static TitleViewModel Create(Title t)
        {
            if (null == t)
            {
                return null;
            }

            TitleViewModel toRet = new TitleViewModel()
            {
                TitleId = t.TitleId,
                TitleName = t.TitleName,
                TitleNameSortable = t.TitleNameSortable,
                TitleTypeId = t.TitleTypeId,
                ReleaseYear = t.ReleaseYear,
                ProcessedDateTimeUTC = t.ProcessedDateTimeUTC,
                TitleParticipants = new List<TitleParticipantViewModel>(),
                TitleGenres = new List<TitleGenreViewModel>(),
                StoryLines = new List<StoryLineViewModel>(),
                OtherNames = new List<OtherNameViewModel>(),
                Awards = new List<AwardViewModel>()
            };

            if (t.TitleParticipants != null)
            {
                foreach (TitleParticipant tp in t.TitleParticipants)
                {
                    toRet.TitleParticipants.Add(TitleParticipantViewModel.Create(tp));
                }
            }

            if (t.TitleGenres != null)
            {
                foreach (TitleGenre tg in t.TitleGenres)
                {
                    toRet.TitleGenres.Add(TitleGenreViewModel.Create(tg));
                }
            }

            if (t.StoryLines != null)
            {
                foreach (StoryLine sl in t.StoryLines)
                {
                    toRet.StoryLines.Add(StoryLineViewModel.Create(sl));
                }
            }

            if (t.OtherNames != null)
            {
                foreach (OtherName theOtherName in t.OtherNames)
                {
                    toRet.OtherNames.Add(OtherNameViewModel.Create(theOtherName));
                }
            }

            if (t.Awards != null)
            {
                foreach (Award a in t.Awards)
                {
                    toRet.Awards.Add(AwardViewModel.Create(a));
                }
            }

            return toRet;
        }

        private TitleViewModel()
        {

        }
    }
}