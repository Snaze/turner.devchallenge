﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Turner.DevChallenge.Model;

namespace Turner.DevChallenge.Web.ViewModels
{
    public class TitleDetailViewModel
    {
        public TitleViewModel Title { get; set; }

        public static TitleDetailViewModel Create(Title theTitle)
        {
            TitleDetailViewModel toRet = new TitleDetailViewModel();
            
            toRet.Title = TitleViewModel.Create(theTitle);

            return toRet;
        }

        private TitleDetailViewModel()
        {

        }
    }
}