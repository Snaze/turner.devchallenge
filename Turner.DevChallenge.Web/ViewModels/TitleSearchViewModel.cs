﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Turner.DevChallenge.Model;

namespace Turner.DevChallenge.Web.ViewModels
{
    public class TitleSearchViewModel
    {
        public List<TitleViewModel> Titles { get; set; }

        public static TitleSearchViewModel Create(IEnumerable<Title> theTitles)
        {
            TitleSearchViewModel toRet = new TitleSearchViewModel();
            toRet.Titles = new List<TitleViewModel>();

            if (null != theTitles)
            {
                foreach (Title t in theTitles)
                {
                    toRet.Titles.Add(TitleViewModel.Create(t));
                }
            }

            return toRet;
        }

        private TitleSearchViewModel()
        {

        }
    }
}