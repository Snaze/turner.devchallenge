﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Turner.DevChallenge.Model;

namespace Turner.DevChallenge.Web.ViewModels
{
    public class TitleParticipantViewModel
    {
        public int Id { get; set; }

        public int TitleId { get; set; }

        public int ParticipantId { get; set; }

        public bool IsKey { get; set; }

        public String RoleType { get; set; }

        public bool IsOnScreen { get; set; }

        public String Name { get; set; }

        public String ParticipantType { get; set; }

        public static TitleParticipantViewModel Create(TitleParticipant tp)
        {
            if (null == tp)
            {
                return null;
            }

            TitleParticipantViewModel toRet = new TitleParticipantViewModel()
            {
                Id = tp.Id,
                TitleId = tp.TitleId,
                ParticipantId = tp.ParticipantId,
                IsKey = tp.IsKey,
                RoleType = tp.RoleType,
                IsOnScreen = tp.IsOnScreen
            };

            if (null != tp.Participant)
            {
                toRet.Name = tp.Participant.Name;
                toRet.ParticipantType = tp.Participant.ParticipantType;
            }

            return toRet;
        }

        private TitleParticipantViewModel()
        {

        }
    }
}