﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Turner.DevChallenge.Model;

namespace Turner.DevChallenge.Web.ViewModels
{
    public class TitleGenreViewModel
    {
        public int Id { get; set; }

        public int TitleId { get; set; }

        public int GenreId { get; set; }

        public String Name { get; set; }

        public static TitleGenreViewModel Create(TitleGenre tg)
        {
            if (null == tg)
            {
                return null;
            }

            TitleGenreViewModel toRet = new TitleGenreViewModel()
            {
                Id = tg.Id,
                TitleId = tg.TitleId,
                GenreId = tg.GenreId
            };

            if (null != tg.Genre)
            {
                toRet.Name = tg.Genre.Name;
            }

            return toRet;
        }

        private TitleGenreViewModel()
        {

        }
    }
}