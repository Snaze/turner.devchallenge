﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Turner.DevChallenge.Model;

namespace Turner.DevChallenge.Web.ViewModels
{
    public class AwardViewModel
    {
        public int TitleId { get; set; }

        public bool? AwardWon { get; set; }

        public int? AwardYear { get; set; }

        public String Award { get; set; }

        public String AwardCompany { get; set; }

        public int Id { get; set; }

        public static AwardViewModel Create(Award theAward)
        {
            if (null == theAward)
            {
                return null;
            }

            AwardViewModel toRet = new AwardViewModel()
            {
                TitleId = theAward.TitleId,
                AwardWon = theAward.AwardWon,
                AwardYear = theAward.AwardYear,
                Award = theAward.Award1,
                AwardCompany = theAward.AwardCompany,
                Id = theAward.Id
            };

            return toRet;
        }

        private AwardViewModel()
        {

        }
    }
}