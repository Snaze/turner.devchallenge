﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Turner.DevChallenge.Model;

namespace Turner.DevChallenge.Web.ViewModels
{
    public class StoryLineViewModel
    {
        public int Id { get; set; }

        public int TitleId { get; set; }

        public String Type { get; set; }

        public String Language { get; set; }

        public String Description { get; set; }

        public static StoryLineViewModel Create(StoryLine sl)
        {
            if (null == sl)
            {
                return null;
            }

            StoryLineViewModel toRet = new StoryLineViewModel()
            {
                Id = sl.Id,
                TitleId = sl.TitleId,
                Type = sl.Type,
                Language = sl.Language,
                Description = sl.Description
            };

            return toRet;
        }

        private StoryLineViewModel()
        {

        }
    }
}