﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Turner.DevChallenge.Model;

namespace Turner.DevChallenge.Web.ViewModels
{
    public class OtherNameViewModel
    {
        public int Id { get; set; }

        public int? TitleId { get; set; }

        public String TitleNameLanguage { get; set; }

        public String TitleNameType { get; set; }

        public String TitleNameSortable { get; set; }

        public String TitleName { get; set; }

        public static OtherNameViewModel Create(OtherName theOtherName)
        {
            if (null == theOtherName)
            {
                return null;
            }

            OtherNameViewModel toRet = new OtherNameViewModel()
            {
                Id = theOtherName.Id,
                TitleId = theOtherName.TitleId,
                TitleNameLanguage = theOtherName.TitleNameLanguage,
                TitleNameType = theOtherName.TitleNameType,
                TitleNameSortable = theOtherName.TitleNameSortable,
                TitleName = theOtherName.TitleName
            };

            return toRet;
        }

        private OtherNameViewModel()
        {

        }
    }
}