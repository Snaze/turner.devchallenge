﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Turner.DevChallenge.DataAccess.Interfaces;
using Turner.DevChallenge.Model;
using Turner.DevChallenge.Web.ViewModels;

namespace Turner.DevChallenge.Web.Controllers
{
    public class HomeController : Controller
    {
        private ITitleDao _TitleDao;

        public HomeController(ITitleDao titleDao)
        {
            _TitleDao = titleDao;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult RetrieveTitleName(string term, int maxCount = 10)
        {
            IEnumerable<string> titleNames = this._TitleDao.RetrieveTitleNames(term, maxCount);
            JsonResult toRet = Json(titleNames, JsonRequestBehavior.AllowGet);
            return toRet;
        }

        [HttpGet]
        public JsonResult RetrieveTitles(string startsWith, int maxCount = 50)
        {
            IEnumerable<Title> titles = this._TitleDao.RetrieveTitles(startsWith, maxCount);
            TitleSearchViewModel viewModel = TitleSearchViewModel.Create(titles);
            JsonResult toRet = Json(viewModel, JsonRequestBehavior.AllowGet);
            return toRet;
        }

        [HttpGet]
        public JsonResult GetTitleDetail(int titleId)
        {
            Title theTitle = this._TitleDao.GetTitleDetail(titleId);
            TitleDetailViewModel viewModel = TitleDetailViewModel.Create(theTitle);
            JsonResult toRet = Json(viewModel, JsonRequestBehavior.AllowGet);
            return toRet;
        }
    }
}