﻿using System.Web;
using System.Web.Optimization;

namespace Turner.DevChallenge.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-1.10.2.js",
                        "~/Scripts/jquery-ui-1.10.4.js",
                        "~/Scripts/jquery-dateFormat.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/knockout").Include(
                        "~/Scripts/knockout-3.1.0.js",
                        "~/Scripts/knockout.mapping-latest.js",
                        "~/Scripts/knockout-jqueryui.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/viewmodel").Include(
                "~/Scripts/ViewModels/TitleSearchViewModel.js",
                "~/Scripts/ViewModels/TitleDetailViewModel.js",
                "~/Scripts/ViewModels/TitleViewModel.js",
                "~/Scripts/ViewModels/TitleParticipantViewModel.js",
                "~/Scripts/ViewModels/TitleGenreViewModel.js",
                "~/Scripts/ViewModels/StoryLineViewModel.js",
                "~/Scripts/ViewModels/OtherNameViewModel.js",
                "~/Scripts/ViewModels/AwardViewModel.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css",
                      "~/Content/themes/base/jquery-ui.css"));
        }
    }
}
