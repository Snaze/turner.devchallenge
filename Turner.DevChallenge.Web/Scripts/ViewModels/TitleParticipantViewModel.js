﻿(function () { 'use strict'; }());

/*global ko:false*/

function TitleParticipantViewModel(tp) {
    this.Id = ko.observable(tp.Id);
    this.TitleId = ko.observable(tp.TitleId);
    this.ParticipantId = ko.observable(tp.ParticipantId);
    this.IsKey = ko.observable(tp.IsKey);
    this.RoleType = ko.observable(tp.RoleType);
    this.IsOnScreen = ko.observable(tp.IsOnScreen);
    this.Name = ko.observable(tp.Name);
    this.ParticipantType = ko.observable(tp.ParticipantType);
}