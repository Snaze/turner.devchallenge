﻿(function () { 'use strict'; }());

/*global ko:false,
    TitleViewModel:false,
    TitleDetailViewModel:false,
    $:false,
    TitleParticipantViewModel:false,
    TitleGenreViewModel:false,
    StoryLineViewModel:false,
    OtherNameViewModel:false,
    AwardViewModel:false*/

function TitleViewModel(title) {
    var self = this, i = 0;

    self.TitleId = ko.observable(title.TitleId);
    self.TitleName = ko.observable(title.TitleName);
    self.TitleNameSortable = ko.observable(title.TitleNameSortable);
    self.TitleTypeId = ko.observable(title.TitleTypeId);
    self.ReleaseYear = ko.observable(title.ReleaseYear);
    self.ProcessedDateTimeUTC = ko.observable(title.ProcessedDateTimeUTC);
    self.TitleParticipants = ko.observableArray([]);
    self.TitleGenres = ko.observableArray([]);
    self.StoryLines = ko.observableArray([]);
    self.OtherNames = ko.observableArray([]);
    self.Awards = ko.observableArray([]);

    if ((title.TitleParticipants !== undefined) &&
             (title.TitleParticipants !== null)) {
        for (i = 0; i < title.TitleParticipants.length; i += 1) {
            self.TitleParticipants.push(
                new TitleParticipantViewModel(title.TitleParticipants[i])
            );
        }
    }

    if ((title.TitleGenres !== undefined) &&
            (title.TitleGenres !== null)) {

        for (i = 0; i < title.TitleGenres.length; i += 1) {
            self.TitleGenres.push(
                new TitleGenreViewModel(title.TitleGenres[i])
            );
        }
    }

    if ((title.StoryLines !== undefined) &&
            (title.StoryLines !== null)) {

        for (i = 0; i < title.StoryLines.length; i += 1) {
            self.StoryLines.push(new StoryLineViewModel(title.StoryLines[i]));
        }
    }

    if ((title.OtherNames !== undefined) &&
            (title.OtherNames !== null)) {

        for (i = 0; i < title.OtherNames.length; i += 1) {
            self.OtherNames.push(new OtherNameViewModel(title.OtherNames[i]));
        }
    }

    if ((title.Awards !== undefined) &&
            (title.Awards !== null)) {

        for (i = 0; i < title.Awards.length; i += 1) {
            self.Awards.push(new AwardViewModel(title.Awards[i]));
        }
    }
}