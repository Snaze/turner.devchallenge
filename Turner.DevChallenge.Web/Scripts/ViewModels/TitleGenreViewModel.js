﻿(function () { 'use strict'; }());

/*global ko:false*/

function TitleGenreViewModel(sl) {
    this.Id = ko.observable(sl.Id);
    this.TitleId = ko.observable(sl.TitleId);
    this.GenreId = ko.observable(sl.GenreId);
    this.Name = ko.observable(sl.Name);
}