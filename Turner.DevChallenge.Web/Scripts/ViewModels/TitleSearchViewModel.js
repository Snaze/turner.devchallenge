﻿(function () { 'use strict'; }());

/*global ko:false, TitleViewModel:false, TitleDetailViewModel:false, $:false*/

function TitleSearchViewModel(retrieveTitlesUrl, getTitleDetailUrl) {
    var self = this, mapping = {
        'Titles': {
            create: function (options) {
                var toRet = new TitleViewModel(options.data);
                return toRet;
            }
        },
        'Title': {
            create: function (options) {
                var toRet = new TitleDetailViewModel(options.data);
                return toRet;
            }
        }
    };

    self.selected = ko.observable('');
    self.Titles = ko.observableArray([]);
    self.Title = ko.observable(new TitleDetailViewModel({
        Title: {
            TitleId: '',
            TitleName: '',
            TitleTypeId: '',
            ReleaseYear: '',
            ProcessedDateTimeUTC: ''
        }
    }));
    self.isOpen = ko.observable(false);
    self.width = ko.observable(640);
    self.height = ko.observable(640);

    self.search = function () {
        var url = retrieveTitlesUrl + '?startsWith='
                                    + encodeURIComponent(self.selected());

        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'json',
            cache: false
        }).done(function (data) {
            ko.mapping.fromJS(data, mapping, self);
        });
    };

    self.viewDetail = function (title) {
        var url = getTitleDetailUrl + '?titleId='
                                    + encodeURIComponent(title.TitleId().toString());

        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'json',
            cache: false
        }).done(function (data) {
            self.Title(new TitleDetailViewModel(data));
            self.isOpen(true);
        });
    };

    self.getLocalProcessedDate = function (title) {

        if ('undefined' === typeof (title.ProcessedDateTimeUTC)) {
            return '';
        }

        var date = new Date(parseInt(title.ProcessedDateTimeUTC().substr(6), 10)),
            toRet = $.format.toBrowserTimeZone(date, "MM/dd/yyyy hh:mm:ss a");

        return toRet;
    };

    /*jslint unparam: true*/
    self.onSelect = function (ev, ui) {
        this.selected(ui.item.value);
    };
    /*jslint unparam: false*/
}