﻿(function () { 'use strict'; }());

/*global ko:false*/

function AwardViewModel(a) {
    this.TitleId = ko.observable(a.TitleId);
    this.AwardWon = ko.observable(a.AwardWon);
    this.AwardYear = ko.observable(a.AwardYear);
    this.Award = ko.observable(a.Award);
    this.AwardCompany = ko.observable(a.AwardCompany);
    this.Id = ko.observable(a.Id);
}