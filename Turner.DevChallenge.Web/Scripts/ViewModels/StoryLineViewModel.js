﻿(function () { 'use strict'; }());

/*global ko:false*/

function StoryLineViewModel(sl) {
    this.Id = ko.observable(sl.Id);
    this.TitleId = ko.observable(sl.TitleId);
    this.Type = ko.observable(sl.Type);
    this.Language = ko.observable(sl.Language);
    this.Description = ko.observable(sl.Description);
}