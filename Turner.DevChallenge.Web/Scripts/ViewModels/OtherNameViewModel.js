﻿(function () { 'use strict'; }());

/*global ko:false*/

function OtherNameViewModel(otherName) {
    this.Id = ko.observable(otherName.Id);
    this.TitleId = ko.observable(otherName.TitleId);
    this.TitleNameLanguage = ko.observable(otherName.TitleNameLanguage);
    this.TitleNameType = ko.observable(otherName.TitleNameType);
    this.TitleNameSortable = ko.observable(otherName.TitleNameSortable);
    this.TitleName = ko.observable(otherName.TitleName);
}