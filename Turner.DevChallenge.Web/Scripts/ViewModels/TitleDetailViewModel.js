﻿(function () { 'use strict'; }());

/*global ko:false, TitleViewModel:false*/

function TitleDetailViewModel(titleSearch) {
    this.Title = ko.observable(new TitleViewModel(titleSearch.Title));
}