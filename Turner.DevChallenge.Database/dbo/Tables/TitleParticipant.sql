﻿CREATE TABLE [dbo].[TitleParticipant] (
    [Id]            INT            IDENTITY (6058, 1) NOT NULL,
    [TitleId]       INT            NOT NULL,
    [ParticipantId] INT            NOT NULL,
    [IsKey]         BIT            NOT NULL,
    [RoleType]      NVARCHAR (100) NULL,
    [IsOnScreen]    BIT            NOT NULL,
    CONSTRAINT [TitleParticipant_PK_TitleParticipant] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [TitleParticipant_FK_TitleParticipant_Participant] FOREIGN KEY ([ParticipantId]) REFERENCES [dbo].[Participant] ([Id]),
    CONSTRAINT [TitleParticipant_FK_TitleParticipant_Title] FOREIGN KEY ([TitleId]) REFERENCES [dbo].[Title] ([TitleId])
);

