﻿CREATE TABLE [dbo].[Genre] (
    [Id]   INT            NOT NULL,
    [Name] NVARCHAR (100) NULL,
    CONSTRAINT [Genre_PK_Genre] PRIMARY KEY CLUSTERED ([Id] ASC)
);

