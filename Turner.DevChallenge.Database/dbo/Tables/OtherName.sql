﻿CREATE TABLE [dbo].[OtherName] (
    [TitleId]           INT            NULL,
    [TitleNameLanguage] NVARCHAR (100) NULL,
    [TitleNameType]     NVARCHAR (100) NULL,
    [TitleNameSortable] NVARCHAR (100) NULL,
    [TitleName]         NVARCHAR (100) NULL,
    [Id]                INT            IDENTITY (76, 1) NOT NULL,
    CONSTRAINT [OtherName_PK__OtherName__00000000000000AB] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [OtherName_FK_OtherName_Title] FOREIGN KEY ([TitleId]) REFERENCES [dbo].[Title] ([TitleId])
);

