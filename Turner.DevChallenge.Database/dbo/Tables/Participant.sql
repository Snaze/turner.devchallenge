﻿CREATE TABLE [dbo].[Participant] (
    [Id]              INT            NOT NULL,
    [Name]            NVARCHAR (100) NULL,
    [ParticipantType] NVARCHAR (100) NULL,
    CONSTRAINT [Participant_PK_Participant] PRIMARY KEY CLUSTERED ([Id] ASC)
);

