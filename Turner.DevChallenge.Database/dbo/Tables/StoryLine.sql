﻿CREATE TABLE [dbo].[StoryLine] (
    [TitleId]     INT            NOT NULL,
    [Type]        NVARCHAR (100) NULL,
    [Language]    NVARCHAR (100) NULL,
    [Description] NTEXT          NULL,
    [Id]          INT            IDENTITY (67, 1) NOT NULL,
    CONSTRAINT [StoryLine_PK__StoryLine__00000000000000A4] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [StoryLine_FK_StoryLine_Title] FOREIGN KEY ([TitleId]) REFERENCES [dbo].[Title] ([TitleId])
);

