﻿CREATE TABLE [dbo].[Award] (
    [TitleId]      INT            NOT NULL,
    [AwardWon]     BIT            NULL,
    [AwardYear]    INT            NULL,
    [Award]        NVARCHAR (100) NULL,
    [AwardCompany] NVARCHAR (100) NULL,
    [Id]           INT            IDENTITY (211, 1) NOT NULL,
    CONSTRAINT [Award_PK__Award__0000000000000083] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [Award_FK_Award_Title] FOREIGN KEY ([TitleId]) REFERENCES [dbo].[Title] ([TitleId])
);

