﻿CREATE TABLE [dbo].[TitleGenre] (
    [Id]      INT IDENTITY (114, 1) NOT NULL,
    [TitleId] INT NOT NULL,
    [GenreId] INT NOT NULL,
    CONSTRAINT [TitleGenre_PK_TitleGenre] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [TitleGenre_FK_TitleGenre_Genre] FOREIGN KEY ([GenreId]) REFERENCES [dbo].[Genre] ([Id]),
    CONSTRAINT [TitleGenre_FK_TitleGenre_Title] FOREIGN KEY ([TitleId]) REFERENCES [dbo].[Title] ([TitleId])
);

