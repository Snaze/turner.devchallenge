﻿CREATE TABLE [dbo].[Title] (
    [TitleId]              INT            NOT NULL,
    [TitleName]            NVARCHAR (100) NULL,
    [TitleNameSortable]    NVARCHAR (100) NULL,
    [TitleTypeId]          INT            NULL,
    [ReleaseYear]          INT            NULL,
    [ProcessedDateTimeUTC] DATETIME       NULL,
    CONSTRAINT [Title_PK_Title] PRIMARY KEY CLUSTERED ([TitleId] ASC)
);

