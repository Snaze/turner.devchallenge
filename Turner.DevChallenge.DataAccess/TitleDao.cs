﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Turner.DevChallenge.DataAccess.Interfaces;
using Turner.DevChallenge.Model;

namespace Turner.DevChallenge.DataAccess
{
    public class TitleDao : ITitleDao
    {
        private TitlesEntities _Context;

        public TitleDao(TitlesEntities context)
        {
            _Context = context;
        }

        public IEnumerable<string> RetrieveTitleNames(String startsWith, int count)
        {
            IEnumerable<string> toRet = (from t in _Context.Titles
                                         where t.TitleName.StartsWith(startsWith)
                                         orderby t.TitleNameSortable
                                         select t.TitleName).Take(count).ToList();

            return toRet;
        }

        public IEnumerable<Title> RetrieveTitles(string startsWith, int count)
        {
            IEnumerable<Title> toRet = (from t in _Context.Titles
                                        where t.TitleName.StartsWith(startsWith)
                                        orderby t.TitleNameSortable
                                        select t).Take(count).ToList();

            return toRet;
        }

        public Title GetTitleDetail(int titleId)
        {
            Title toRet = (from t in _Context.Titles.Include("TitleParticipants.Participant")
                                                    .Include("TitleGenres.Genre")
                                                    .Include("StoryLines")
                                                    .Include("OtherNames")
                                                    .Include("Awards")
                           where t.TitleId == titleId
                           orderby t.TitleNameSortable
                           select t).FirstOrDefault();

            return toRet;
        }
    }
}
