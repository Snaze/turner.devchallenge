﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Turner.DevChallenge.Model;

namespace Turner.DevChallenge.DataAccess.Interfaces
{
    public interface ITitleDao
    {
        IEnumerable<String> RetrieveTitleNames(String startsWith, int count);

        IEnumerable<Title> RetrieveTitles(String startsWith, int count);

        Title GetTitleDetail(int titleId);
    }
}
