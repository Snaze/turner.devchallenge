Here is my Dev Challenge.

I created a SQL Server authenticated login to access the database. (TitlesUser / password)

The website is ASP.NET MVC 5.  I tested only with the Visual Studio IIS Express.
The data access is EF 6.1.
The unit tests use MS Test and Moq.  I only unit tested the controller for the sake of time.
The UI uses knockout JS.  
The DI container is NInject.

There is a database project as well.  I always try to keep my schema changes in source control.  Kind of pointless for this 
exercise since I made no schema changes but, I wanted to demonstrate how I like to usually work.

Anyways, thank you for the consideration!

-Scott