﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Turner.DevChallenge.Web;
using Turner.DevChallenge.Web.Controllers;
using Moq;
using Turner.DevChallenge.DataAccess.Interfaces;
using Turner.DevChallenge.Model;
using Turner.DevChallenge.Web.ViewModels;

namespace Turner.DevChallenge.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        private Mock<ITitleDao> _TitleDao;
        private HomeController _HomeController;
        private IEnumerable<Title> _Titles;

        [TestInitialize]
        public void Setup()
        {
            _TitleDao = new Mock<ITitleDao>();
            _HomeController = new HomeController(_TitleDao.Object);
            _Titles = CreateDummyTitles();
        }

        private IEnumerable<Title> CreateDummyTitles()
        {
            IList<Title> toRet = new List<Title>();
            toRet.Add(new Title()
            {
                Awards = new List<Award>(new [] { 
                    new Award() { 
                        TitleId = 1, 
                        Award1 = "Best Picture", 
                        AwardCompany = "Universal", 
                        AwardWon = true, 
                        AwardYear = 2010, 
                        Id = 1}}),
                OtherNames = new List<OtherName>(),
                ProcessedDateTimeUTC = DateTime.UtcNow,
                ReleaseYear = 2010,
                StoryLines = new List<StoryLine>(),
                TitleGenres = new List<TitleGenre>(),
                TitleId = 1,
                TitleName = "Dummy Title 1",
                TitleNameSortable = "Dummy Title 1",
                TitleParticipants = new List<TitleParticipant>(),
                TitleTypeId = 1
            });

            toRet.Add(new Title()
            {
                Awards = new List<Award>(),
                OtherNames = new List<OtherName>(),
                ProcessedDateTimeUTC = DateTime.UtcNow,
                ReleaseYear = 2008,
                StoryLines = new List<StoryLine>(),
                TitleGenres = new List<TitleGenre>(),
                TitleId = 2,
                TitleName = "Dummy Title 2",
                TitleNameSortable = "Dummy Title 2",
                TitleParticipants = new List<TitleParticipant>(),
                TitleTypeId = 1
            });

            toRet.Add(new Title()
            {
                Awards = new List<Award>(),
                OtherNames = new List<OtherName>(),
                ProcessedDateTimeUTC = DateTime.UtcNow,
                ReleaseYear = 2006,
                StoryLines = new List<StoryLine>(),
                TitleGenres = new List<TitleGenre>(),
                TitleId = 3,
                TitleName = "Dummy Title 3",
                TitleNameSortable = "Dummy Title 3",
                TitleParticipants = new List<TitleParticipant>(),
                TitleTypeId = 1
            });

            toRet.Add(new Title()
            {
                Awards = new List<Award>(),
                OtherNames = new List<OtherName>(),
                ProcessedDateTimeUTC = DateTime.UtcNow,
                ReleaseYear = 2004,
                StoryLines = new List<StoryLine>(),
                TitleGenres = new List<TitleGenre>(),
                TitleId = 4,
                TitleName = "Dummy Title 4",
                TitleNameSortable = "Dummy Title 4",
                TitleParticipants = new List<TitleParticipant>(),
                TitleTypeId = 1
            });

            return toRet;
        }

        [TestCleanup]
        public void TearDown()
        {
            _HomeController.Dispose();
            _HomeController = null;
            _TitleDao = null;
        }

        [TestMethod]
        public void RetrieveTitleName_ShouldRetrieveResults_FromTitleDao()
        {
            String[] titleResults = new[] { "Title1", "Title2", "Title3" };
            String startsWithString = "Ti";
            int maxCount = 10;

            // Arrange
            _TitleDao.Setup(t => t.RetrieveTitleNames(It.Is<String>(s => s == startsWithString), It.Is<int>(i => i == maxCount))).Returns(() => titleResults).Verifiable();

            // Act
            JsonResult result = _HomeController.RetrieveTitleName(startsWithString, maxCount);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsTrue(result.Data is IEnumerable<String>);
            List<string> resultList = ((IEnumerable<String>)result.Data).ToList();
            Assert.IsTrue(resultList.Contains("Title1"));
            Assert.IsTrue(resultList.Contains("Title2"));
            Assert.IsTrue(resultList.Contains("Title3"));
            _TitleDao.Verify();
        }

        [TestMethod]
        public void TitlesSearch_ShouldRetrieveTitles_StartingWithTheEnteredTitle()
        {
            String startsWithString = "Dummy";
            int maxCount = 10;

            // ARRANGE
            _TitleDao.Setup(t => t.RetrieveTitles(It.Is<String>(s => s.ToLower().StartsWith("dummy")), It.IsAny<int>())).Returns(() => _Titles).Verifiable();

            // ACT
            JsonResult result = _HomeController.RetrieveTitles(startsWithString, maxCount);

            // ASSERT
            Assert.IsNotNull(result);
            Assert.IsTrue(result.Data is TitleSearchViewModel);
            List<TitleViewModel> results = ((TitleSearchViewModel)result.Data).Titles;
            List<int> titleIds = results.Select(r => r.TitleId).ToList();
            Assert.IsTrue(titleIds.Contains(1));
            Assert.IsTrue(titleIds.Contains(2));
            Assert.IsTrue(titleIds.Contains(3));
            Assert.IsTrue(titleIds.Contains(4));
            _TitleDao.Verify();
        }

        [TestMethod]
        public void GetTitleDetail_ShouldGetTitleDetail_FromTitleDao()
        {
            int titleId = 1;

            // ARRANGE
            _TitleDao.Setup(t => t.GetTitleDetail(It.Is<int>(i => i.Equals(1)))).Returns(() => _Titles.Where(t => t.TitleId == 1).FirstOrDefault()).Verifiable();

            // ACT
            JsonResult result = _HomeController.GetTitleDetail(titleId);

            // ASSERT
            Assert.IsNotNull(result);
            Assert.IsTrue(result.Data is TitleDetailViewModel);
            _TitleDao.Verify();
        }
    }
}
